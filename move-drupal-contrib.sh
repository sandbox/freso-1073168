#!/bin/sh
# Run this from the Drupal install's root folder, after having cloned core

cvsdir=$1
types=module
#types="module theme" # Uncomment this line if you use themes from CVS.

for t in $types; do
  contribdir=$cvsdir/sites/all/${t}\s # Adds an s for dir name.
  for i in `ls $contribdir`; do
    if [ $i != 'CVS' ] && [ -d "$contribdir/$i" ]; then
      git submodule add http://git.drupal.org/project/$i.git sites/all/${t}s/$i
      git commit -a -m "Added $i $t."
    fi
  done
done
