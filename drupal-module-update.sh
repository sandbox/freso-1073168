#!/bin/sh
GIT=`which git`
CONTRIBDIR='sites/all/modules'
BASEDIR=`pwd`
USAGE="Usage: $0 project [-t tag]"

if [ ! -d '.git' ]; then
  echo 'Warning: No ".git" directory found.'
  echo 'This script only makes sense when run from the root of a Git repository.'
  exit 1
fi

if [ -z "$1" ]; then
  echo "No arguments provided!"
  echo $USAGE
  exit
elif [ "$1" = '--help' ] || [ "$1" = '-h' ]; then
  echo $USAGE
  exit
else
  PROJECT=$1
  echo "Project: $PROJECT"
fi

# Go to project's directory
cd $CONTRIBDIR/$1

if [ $# -eq 3 ]; then
  case $2 in
    '-t')
      TAG=$3
      echo "Tag: $TAG"
      git fetch
      git checkout $3
      ;;
    *)
      echo "Invalid option!"
      echo $USAGE
      exit
      ;;
  esac
elif [ $# -eq 1 ]; then
  git pull --rebase
else
  echo "Bad number of arguments provided."
  echo $USAGE
  exit 1
fi

# Post-update clean-up
git gc
