#!/bin/sh
GIT=`which git`
DRUPALREPOS='http://git.drupal.org/project'
CONTRIBDIR='sites/all/modules'

USAGE="Usage: $0 project [-t tag|-b branch]"

if [ ! -d '.git' ]; then
  echo 'Warning: No ".git" directory found.'
  echo 'This script only makes sense when run from the root of a Git repository.'
  exit 1
fi

clone_project () {
  if [ ! $# -eq 1 ] && [ ! $# -eq 3 ]; then
    echo "Bad number of arguments used to call clone_project()."
    return 0
  fi

  git_command="$GIT submodule add"
  if [ "$2" = 'branch' ]; then
    git_command="$git_command --branch $3"
    use_tag=0
  fi
  git_command="$git_command $DRUPALREPOS/$1.git $CONTRIBDIR/$1"
  $git_command

  if [ ! $? -eq 0 ]; then
    echo "Running \"$git_command $1\" failed. Check logs and output."
    exit 1
  fi

  if [ "$2" = 'tag' ]; then
    cd $CONTRIBDIR/$1
    git checkout $3
  fi;

  return $?
}

if [ -z "$1" ]; then
  echo "No arguments provided!"
  echo $USAGE
  exit
elif [ "$1" = '--help' ] || [ "$1" = '-h' ]; then
  echo $USAGE
  exit
else
  PROJECT=$1
  echo "Project: $PROJECT"
fi

if [ $# -eq 3 ]; then
  case $2 in
    '-t')
      TAG=$3
      echo "Tag: $TAG"
      clone_project $1 tag $3
      ;;
    '-b')
      BRANCH=$3
      echo "Branch: $BRANCH"
      clone_project $1 branch $3
      ;;
    *)
      echo "Invalid option!"
      echo $USAGE
      exit
      ;;
  esac
elif [ $# -eq 1 ]; then
  $GIT submodule add $DRUPALREPOS/$1.git $CONTRIBDIR/$1
else
  echo "Bad number of arguments provided."
  echo $USAGE
  exit 1
fi
